var gulp = require('gulp');
var concat = require('gulp-concat');
var runSequence = require('run-sequence');
var uglify = require('gulp-uglify');

var paths = {
  libs: [
//    './bower_components/jquery/dist/jquery.js',
//    './bower_components/angular/angular.js',
//    './bower_components/bootstrap/dist/js/bootstrap.js',
//    './bower_components/angular-animate/angular-animate.js',
//    './bower_components/angular-cookies/angular-cookies.js',
//    './bower_components/angular-resource/angular-resource.js',
//    './bower_components/angular-route/angular-route.js',
//    './bower_components/angular-sanitize/angular-sanitize.js',
//    './bower_components/angular-touch/angular-touch.js'
  ],
  angularLibs: [
    './scripts/controllers/about.js',
    './scripts/controllers/main.js',
    './scripts/controllers/newPages.js',
    './scripts/app.js'
  ],
  css:    [
    './bower_components/bootstrap/dist/css/bootstrap.css',
    './styles/main.css'
  ]
};

gulp.task('libs', function () {
  gulp.src(paths.libs)
    .pipe(concat('libs.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./app/scripts/gulp'));
});
gulp.task('angularLibs', function () {
  gulp.src(paths.libs)
    .pipe(concat('angularLibs.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./app/scripts/gulp'));
});
gulp.task('css', function () {
  gulp.src(paths.css)
    .pipe(concat('libs.css'))
    .pipe(gulp.dest('./app/style/gulpCss'));
});

gulp.task('default', ['css','libs','angularLibs']);
gulp.task('serve', function() {
  return runSequence('css','libs','angularLibs','server');
});