'use strict';

/**
 * @ngdoc function
 * @name angularLikeBoostooApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the angularLikeBoostooApp
 */
angular.module('angularLikeBoostooApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
