'use strict';

/**
 * @ngdoc function
 * @name angularLikeBoostooApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the angularLikeBoostooApp
 */

angular.module('angularLikeBoostooApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
