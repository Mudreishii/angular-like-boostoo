angular.module('angularLikeBoostooApp')

  .directive('header', function(){
    return {
      restrict: 'E',
      controller: 'headerControll',
      templateUrl: '../templates/header.html'
    };
  })
  .directive('leftmenu', function(){
    return {
      restrict: 'E',
      templateUrl: '../templates/sideMenu.html'
    };
  })
  .directive('content', function(){
    return {
      restrict: 'E',
      templateUrl: '../templates/content.html'
    };
  })
  .directive('footer', function(){
    return {
      restrict: 'E',
      templateUrl: '../templates/footer.html'
    }
  });

