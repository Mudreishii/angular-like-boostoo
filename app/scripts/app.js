'use strict';

/**
 * @ngdoc overview
 * @name angularLikeBoostooApp
 * @description
 * # angularLikeBoostooApp
 *
 * Main module of the application.
 */
angular
  .module('angularLikeBoostooApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
//        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
//        controllerAs: 'about'
      })
      .when('/newPages', {
        templateUrl: 'views/newPages.html',
        controller: 'newCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
